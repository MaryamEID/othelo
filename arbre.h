#ifndef ARBRE_H_
#define ARBRE_H_
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "allocation.h"
#include "deroulement_partie.h"

/*Structure d'un noeud de l'arbre
Les fils sont réprésentés sous la forme d'un tableau de noeud
Chaque noeud contient des coordonnées, un plateau, un int correspondant aux nombres de fils*/
typedef struct noeud{
  coordonne c;
  plateau p;
  int nb_fils;
  struct noeud **fils;
}noeud;

typedef noeud *arbre;


/*Fonctions de manipulation d'arbres*/


arbre arbre_vide();

int est_vide(arbre a);

int remplir_arbre(plateau *p,int compteur,joueur *jr, coordonne *coup_possible);

arbre creer_arbre_position(arbre a,configuration config, plateau p, joueur *j, coordonne c,int profondeur,int compteur);

void ajoute_fils(arbre a, int position, arbre fils);

arbre creer_feuille(plateau *p, coordonne c,joueur *j);

void afficher_arbre(arbre a);

void liberer_arbre(arbre a);

#endif
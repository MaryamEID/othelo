#ifndef DEROULEMENT_PARTIE_H_
#define DEROULEMENT_PARTIE_H_

#include <stdio.h>
#include <stdlib.h>
#include "joueur.h"
#include "plateau.h"

typedef struct configuration{
  joueur *j1;
  joueur *j2;

  plateau *p;

}configuration;

int est_dans_plateau(int i, int j, int taille, plateau p);

int coup_valide(configuration config, coordonne c,int compteur,joueur j);

void jouer_coup(joueur *j,plateau p, coordonne c);

int est_finie(configuration config,int compteur);

int position_gagnante(configuration config, int compteur, joueur jr);


#endif

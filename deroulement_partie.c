#include "deroulement_partie.h"



int est_dans_plateau(int i, int j, int taille, plateau p){
  if(i>=0 && i<taille && j>=0 && j<taille && p[i][j]>=0 && p[i][j]<=2){
    return 1;
  }
  return 0;

}


int coup_valide(configuration config, coordonne c,int compteur, joueur jr){
  
  int pions_j=jr.type_pions;
  int pions_adv;
  if(pions_j==NOIR) pions_adv=BLANC;
  else if(pions_j==BLANC) pions_adv=NOIR;



  /*si un pion est déjà posé à ces coordonnés ou si les coordonnees ne correspondent pas à une case du plateau*/
  if((*config.p)[c.i][c.j]!=0 || !(est_dans_plateau(c.i,c.j,N,*config.p)) ){
    return 0;
  }

  /*4 premiers pions*/
  if(compteur<4){
    if((c.i==3||c.i==4) && (c.j==3||c.j==4))
      return 1;
    else
      return 0;
  }

  /*afficher_plateau(*config.p,8);
   */


  /*variable permettant de savoir si l'on est entré dans une boucle while*/
  int vu=0;
  
  

    /*vertical haut*/
  int x=c.i-1;

  while((*config.p)[x][c.j]==pions_adv && est_dans_plateau(x,c.j,N,*config.p)){
    /*printf("vertical haut\n");*/
    x--;
    vu=1;
  }
  /*printf("i: %d\n",x);*/
  if (vu==1 && (*config.p)[x][c.j]==pions_j && est_dans_plateau(x,c.j,N,*config.p)){
    return 1;
  }
    
    /*vertical bas*/
  vu=0;
    x=c.i+1;
    while((*config.p)[x][c.j]==pions_adv && est_dans_plateau(x,c.j,N,*config.p)){
      /*printf("vertical bas\n");*/
      x++;
      vu=1;
    }
    if (vu==1 && (*config.p)[x][c.j]==pions_j && est_dans_plateau(x,c.j,N,*config.p)){
      return 1;
    }

    /*horizontal gauche*/
    vu=0;
    int j=c.j-1;
    while((*config.p)[c.i][j]==pions_adv && est_dans_plateau(c.i,j,N,*config.p)){
      /*printf("horizontal gauche\n");*/
      j--;
      vu=1;
    }
    if (vu==1 && (*config.p)[c.i][j]==pions_j && est_dans_plateau(c.i,j,N,*config.p)){
      return 1;
    }

    /*horizontal droite*/
    j=c.j+1;
    vu=0;
    while((*config.p)[c.i][j]==pions_adv && est_dans_plateau(c.i,j,N,*config.p)){
      /*printf("hz\n");*/
      j++;
      vu=1;
    }
    if (vu==1 && (*config.p)[c.i][j]==pions_j && est_dans_plateau(c.i,j,N,*config.p)){
      return 1;
    }

    /*diagonale gauche haut*/
    j=c.j-1;
    vu=0;
    int i=c.i-1;
    while((*config.p)[i][j]==pions_adv && est_dans_plateau(i,j,N,*config.p)){
      /* printf("dgh\n");*/
      j--;
      i--;
      vu=1;
    }
    if (vu==1 && (*config.p)[i][j]==pions_j && est_dans_plateau(i,j,N,*config.p)){
      return 1;
    }

    /*diagonale droite haut*/
    j=c.j+1;
    i=c.i-1;
    vu=0;
    while((*config.p)[i][j]==pions_adv && est_dans_plateau(i,j,N,*config.p)){
      j++;
      i--;
      vu=1;
    }
    if (vu==1 && (*config.p)[i][j]==pions_j && est_dans_plateau(i,j,N,*config.p)){
      /*printf("ddh\n");*/
      return 1;
    }

    /*diagonale bas gauche*/
    j=c.j-1;
    i=c.i+1;
    vu=0;
    while((*config.p)[i][j]==pions_adv && est_dans_plateau(i,j,N,*config.p)){
      /*printf("dgb\n");*/
      j--;
      i++;
      vu=1;
    }
    if (vu==1 && (*config.p)[i][j]==pions_j && est_dans_plateau(i,j,N,*config.p)){
      return 1;
    }
    
    /*diagonale bas droite*/
    j=c.j+1;
    i=c.i+1;
    vu=0;
    while((*config.p)[i][j]==pions_adv && est_dans_plateau(i,j,N,*config.p)){
      /*printf("dbd\n");*/
      j++;
      i++;
      vu=1;
    }
    if (vu==1 && (*config.p)[i][j]==pions_j && est_dans_plateau(i,j,N,*config.p)){
      return 1;
    }
    
  return 0;

}

void jouer_coup(joueur *jr,plateau p, coordonne c){
  /*
  printf("type:%d\n",j->type_pions);
  printf("%d\n",p[c.i][c.j]);
  printf("%d\n",j->type_pions);
  */
  int pions_j=jr->type_pions;
  int pions_adv;
  int i,j;
  
  /*placement du pion*/
  p[c.i][c.j]=jr->type_pions;


  if(pions_j==NOIR) pions_adv=BLANC;
  else pions_adv=NOIR;


  
  /*vertical haut*/
  i=c.i-1;
  while(p[i][c.j]==pions_adv){
     i-=1;
  }
  if(p[i][c.j]==pions_j && est_dans_plateau(i,c.j,N,p)){
    i++;
    while(p[i][c.j]==pions_adv){
      p[i][c.j]=pions_j;
      i++;
    
    }
  }
   
  /*vertical bas*/
  i=c.i+1;
  while(p[i][c.j]==pions_adv){
    i+=1;
  }
  if(p[i][c.j]==pions_j && est_dans_plateau(i,c.j,N,p)){
    i--;
    while(p[i][c.j]==pions_adv){
      p[i][c.j]=pions_j;
      i--;
     
    }
  }

  /*horizontal droite*/
  j=c.j+1;
  while(p[c.i][j]==pions_adv){
    j+=1;
  }
  if(p[c.i][j]==pions_j && est_dans_plateau(c.i,j,N,p)){
    j--;
    while(p[c.i][j]==pions_adv){
      p[c.i][j]=pions_j;
      j--;
     
    }
  }

  /*horizontale gauche*/
  j=c.j-1;
  while(p[c.i][j]==pions_adv){
    j-=1;
  }
  if(p[c.i][j]==pions_j && est_dans_plateau(c.i,j,N,p)){
    j++;
    while(p[c.i][j]==pions_adv){
      p[c.i][j]=pions_j;
      j++;
     
    }
  }

  /*diagonale gauche haut*/
  i=c.i-1;
  j=c.j-1;
  while(p[i][j]==pions_adv){
    j-=1;
    i-=1;
  }
  if(p[i][j]==pions_j && est_dans_plateau(i,j,N,p)){
    j++;
    i++;
    while(p[i][j]==pions_adv){
      p[i][j]=pions_j;
      j++;
      i++;
    
    }
  }

  /*diagonale droite haut*/
  i=c.i-1;
  j=c.j+1;
  while(p[i][j]==pions_adv){
    j+=1;
    i-=1;
  }
  if(p[i][j]==pions_j && est_dans_plateau(i,j,N,p)){
    j--;
    i++;
    while(p[i][j]==pions_adv){
      p[i][j]=pions_j;
      j--;
      i++;
     
    }
  }
  /*diagonale gauche bas*/
  i=c.i+1;
  j=c.j-1;
  while(p[i][j]==pions_adv){
    j-=1;
    i+=1;
  }
  if(p[i][j]==pions_j && est_dans_plateau(i,j,N,p)){
    j++;
    i--;
    while(p[i][j]==pions_adv){
      p[i][j]=pions_j;
      j++;
      i--;
     
    }
  }
  /*diagonale droite bas*/
  i=c.i+1;
  j=c.j+1;
  while(p[i][j]==pions_adv){
    j+=1;
    i+=1;
  }
  if(p[i][j]==pions_j && est_dans_plateau(i,j,N,p)){
    j--;
    i--;
    while(p[i][j]==pions_adv){
      p[i][j]=pions_j;
      j--;
      i--;
      
    }
  }
  
  /* ajout_pion(jr,c.i,c.j);*/

}

int est_finie(configuration config,int compteur){
  coordonne c;
  for(c.i=0;c.i<N;c.i++){
    for(c.j=0;c.j<N;c.j++){
      /*printf("c.i1: %d c.j1: %d\n",c.i,c.j);*/
      if((*config.p)[c.i][c.j]==0 && (coup_valide(config,c,compteur,*config.j1) || coup_valide(config,c,compteur,*config.j2))){
	return 0;

      }
    }
  }

  return 1;
}



int position_gagnante(configuration config, int compteur,joueur jr){
  coordonne c;
  for(c.i=0;c.i<N;c.i++){
    for(c.j=0;c.j<N;c.j++){
      /* printf("c.i1: %d c.j1: %d\n",c.i,c.j);*/
      if((*config.p)[c.i][c.j]==0 && coup_valide(config,c,compteur,jr)){
	return 1;

      }
    }
  }

  return 0;
}


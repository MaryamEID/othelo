#include "arbre.h"

/*Renvoie un arbre vide*/
arbre arbre_vide(){
    return NULL;
}

/*Détermine si un arbre est vide*/
int est_vide(arbre a){

    return(a==arbre_vide());
}


/**Remplissage d'un tableau contenant tous les coups possibles pour un joueur à partir d'une configuration de jeu
* Détermine le nombre de fils d'un noeud de l'arbre
**/
int remplir_arbre(plateau *p,int compteur,joueur *jr, coordonne *coup_possible){
      coordonne c;
      configuration config_copie;

      config_copie.p=p;
      
  int nb_fils=0;

  for(c.i=0;c.i<N;c.i++){
    for(c.j=0;c.j<N;c.j++){

      if((*config_copie.p)[c.i][c.j]==0 && coup_valide(config_copie,c,compteur,*jr)){
        coup_possible[nb_fils]=c;
        nb_fils++;
        
      }
    }
  }

    return nb_fils;
    
}


/*Création d'un arbre contenant toutes les configurations de jeu possibles à partir de la configuration
actuelle, la profondeur est déterminée par l'utilisateur*/
arbre creer_arbre_position(arbre a,configuration config, plateau p, joueur *jr, coordonne c, int profondeur, int compteur){
    int i;
    coordonne *coup_possible=allocation(sizeof(coordonne),64);



    /*création d'un plateau qui correspondra à une configuration différente pour chaque noeud*/
    plateau p2;
    init_plateau(p2,N);
    

    if(profondeur>0){
    /*La racine part des coups possible pour l'ordinateur*/


    /*Le plateau du jeu en cours est copié dans la racine de l'arbre */
    memcpy(a->p,p,sizeof(plateau));
    /*Remplissage d'un tableau avec tout les coups possibles pour l'ordinateur à partir de la configuration de jeu dans la racine
    Le nombre de possibilités est renvoyé et correspond au nombre de fils de la racine*/
    a->nb_fils=remplir_arbre(&a->p,compteur,jr,coup_possible);
    /*Allocation de l'espace nécessaire pour les fils de la racine*/
    a->fils=(arbre *)allocation(sizeof(arbre),a->nb_fils);



    /*Pour chacun de ces fils, on copie le tableau de la racine puis on place dans le noeud le tableau qui correspond au la configuration du jeu
    si l'on joue le coup coup_possible[i]
    On place aussi comme information dans le noeud les coordonnées de ce coup, le nombre de fils=0  et le tableau fils=NULL*/
    for(i=0;i<a->nb_fils;i++){
        memcpy(p2,*a->p,sizeof(plateau));
        a->fils[i]=creer_feuille(&p2,coup_possible[i],jr);
        
    }
        /*Les coups suivants correspond aux coups du joueur humain et ainsi de suite*/
        if (jr==config.j1) jr=config.j2;
        else if (jr==config.j2) jr=config.j1;
        /*On répète le procédé ci-dessus pour les fils*/

    libere_mem(&coup_possible);
    for(i=0;i<a->nb_fils;i++){
     creer_arbre_position(a->fils[i],config, a->fils[i]->p,jr,c,profondeur-1,compteur+1);
    }
    }
    return a;
}


/*Crée une feuille contenant des coordonnées et un plateau où un pion est placé aux coordonnées entrées
en paramètre*/
arbre creer_feuille(plateau *p, coordonne c,joueur *j){
    arbre a= allocation(sizeof(noeud),1);
    jouer_coup(j,*p,c);
    a->c=c;
    memcpy(a->p,p,sizeof(plateau));

    a->nb_fils=0;
    a->fils=NULL;

    return a;

}


/*Affichage des coordonnées dans l'arbre*/
void afficher_arbre(arbre a){

    if(!est_vide(a)){
        int i;
        printf("i:%d j:%d\n",a->c.i+1,a->c.j+1);

        for(i=0;i<a->nb_fils;i++){
            printf("----------\n");
            afficher_arbre(a->fils[i]);
        }
        
    }

}


/*Libère la place de l'arbre en mémoire*/
void liberer_arbre(arbre a){

    if(!est_vide(a)){
        int i;
        for(i=0;i<a->nb_fils;i++){
            liberer_arbre(a->fils[i]);
        }
        libere_mem(&a);
    }


}
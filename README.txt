Titre: Reversi


Auteurs: Lina Belhadj & Maryam EID


Implémenté en C


Règles du jeu:
Le jeu se joue sur plateau de 64 cases. Les joueurs disposent de 64 pions, blanc d'un côté, noir de l'autre. Le but est de retourner les pions du joueur adverse pour avoir à la fin du jeu le maximum de pions de sa couleur. Ce jeu se joue contre l'ordinateur.
Cliquez sur les cases pour poser vos pions. Vous pouvez sauvegarder et recharger une partie en cours de jeu en cliquant sur les boutons correspondant à ces options.


Installation et usage:
terminal>make
terminal>./reversi -prof <profondeur> -niveau <niveau>
Si les paramètres profondeur et niveau ne sont pas spécifiés, les valeurs choisies par défauts sont 2 pour la profondeur et 4 pour le niveau.
Si le niveau choisi est le niveau 0 ou 1, il n'est pas nécessaire de spécifier de profondeur.



Liste des fichiers du jeu:
-main.c

-deroulement_partie.c
-deroulement_partie.h

-ia.c
-ia.h

-arbre.c
-arbre.h

-menu.c
-menu.h

-interface_mlv.c
-interface.h

-sauvegarde.c
-sauvegarde.h

-joueur.c
-joueur.h

-plateau.c
-plateau.h

-allocation.c
-allocation.h

-sauvegarde.txt

-makefile

#include"interface_mlv.h"
#include<stdlib.h>
#include<stdio.h>
#include <MLV/MLV_all.h>

void tracer_plateau(plateau p){
  
 int i, j;

          
  MLV_draw_filled_rectangle(0,0,600,600, MLV_COLOR_BURLYWOOD4);
  MLV_draw_filled_rectangle(100,100,400,400, MLV_COLOR_GREEN);
 for(i=2;i<10;i++){
   for(j=2;j<10;j++){
     if(p[i-2][j-2]==0){
     MLV_draw_filled_rectangle((i*50),(j*50),50,50, MLV_COLOR_GREEN);
     }
     MLV_draw_line((i*50),100,(i*50),500,MLV_COLOR_BLACK);
     MLV_draw_line(100,(j*50),500,(j*50),MLV_COLOR_BLACK);
     }
    }
}

int nb_pions_blanc(joueur j){
  
 MLV_draw_filled_circle(50,300,25,MLV_COLOR_WHITE);
 MLV_draw_text(48,350,"%d",MLV_COLOR_WHITE,j.pions_total);
 return j.pions_total;
}


int nb_pions_noir(joueur j){
 MLV_draw_filled_circle(550,300,25,MLV_COLOR_BLACK);
 MLV_draw_text(550,350,"%d",MLV_COLOR_WHITE,j.pions_total);
 return j.pions_total;
}



coordonne afficher_coup_joueur(coordonne c,plateau p){
  int clic_x=c.j;
  int clic_y=c.i;
 
  
  MLV_draw_filled_circle((clic_x*50)+125,(clic_y*50)+125,25,MLV_COLOR_BLACK);
  
  return c;
}

coordonne afficher_coup_ordi(coordonne c,plateau p){
  int clic_x=c.j;
  int clic_y=c.i;

  MLV_draw_filled_circle((clic_x*50)+125,(clic_y*50)+125,25,MLV_COLOR_WHITE);

  return c;
}

void change_couleur(plateau p){
  int i,j;
   for(j=0;j<=7;j++){
   for(i=0;i<=7;i++){
     if(p[i][j]==1){
       
       MLV_draw_filled_circle((j*50)+125,(i*50)+125,25,MLV_COLOR_BLACK);
     
     }
     else if(p[i][j]==2){
     
        MLV_draw_filled_circle((j*50)+125,(i*50)+125,25,MLV_COLOR_WHITE);
       
     }
   }
   }
}


void score(configuration config){
  coordonne c;
  int score_j1=0;
  int score_j2=0;
  
  for(c.i=0;c.i<N;c.i++){
    for(c.j=0;c.j<N;c.j++){
      
      if((*config.p)[c.i][c.j]==NOIR){
	score_j1++;

      }
      else if((*config.p)[c.i][c.j]==BLANC){
	score_j2++;
      }
    }
  }
  if(score_j1 > score_j2){
    
    MLV_draw_adapted_text_box(200,200,"YOU WIN :) votre score: %d score ordi:%d",30, MLV_COLOR_GREEN,MLV_COLOR_BLACK,MLV_COLOR_RED,MLV_TEXT_LEFT,score_j1,score_j2);
  }
  else if(score_j2 > score_j1){
    MLV_draw_adapted_text_box(200,200,"GAME OVER :( votre score: %d score ordi:%d",30, MLV_COLOR_GREEN,MLV_COLOR_BLACK,MLV_COLOR_RED,MLV_TEXT_LEFT,score_j1,score_j2);
  }
  else{
    MLV_draw_adapted_text_box(200,200,"EGALITE votre score: %d score ordi:%d",30, MLV_COLOR_GREEN,MLV_COLOR_BLACK,MLV_COLOR_RED,MLV_TEXT_LEFT,score_j1,score_j2);
  }
}




void afficher_menu(int menu){
  
  MLV_draw_text(250,150,"REVERSI",MLV_COLOR_GREEN);
     
 
  if(menu==1){/*affiche le titre dans le menu principal*/
    MLV_draw_text(250,150,"REVERSI",MLV_COLOR_GREEN);
     
  
  }
  
  MLV_Color couleur = MLV_COLOR_GREEN;
  MLV_draw_text(250,200,"Nouvelle partie", couleur);
  MLV_draw_text(250,300,"Charger derniere partie", couleur);
  MLV_draw_text(250,400,"Quitter", couleur);
  

}

void acceder_options(){
  
  MLV_draw_text_box(0,50,100,30,"Sauvegarde",2,MLV_COLOR_RED,MLV_COLOR_RED,MLV_COLOR_YELLOW,MLV_TEXT_CENTER,MLV_HORIZONTAL_CENTER,MLV_VERTICAL_CENTER);

  MLV_draw_text_box(200,50,200,30,"Charger Sauvegarde",2,MLV_COLOR_RED,MLV_COLOR_RED,MLV_COLOR_YELLOW,MLV_TEXT_CENTER,MLV_HORIZONTAL_CENTER,MLV_VERTICAL_CENTER);

  MLV_draw_text_box(500,50,100,30,"Quitter",2,MLV_COLOR_RED,MLV_COLOR_RED,MLV_COLOR_YELLOW,MLV_TEXT_CENTER,MLV_HORIZONTAL_CENTER,MLV_VERTICAL_CENTER);

  
}


void gerer_options(configuration config,int *compteur, int *profondeur, int*niveau, int clic_x, int clic_y){
  

  if(clic_x>0&&clic_x<100){
    if(clic_y>50&&clic_y<80){
     Sauvegarde(config,compteur,profondeur,niveau);
         }
      }

    else if (clic_x>200&&clic_x<400){
      if(clic_y>50&&clic_y<80){
	init2(config,compteur,profondeur, niveau);
       tracer_plateau(*(config.p));
      acceder_options();
     change_couleur(*(config.p));
      nb_pions_blanc(*config.j2);
      nb_pions_noir(*config.j1);
      
      MLV_actualise_window();
      }
    }
    else if(clic_x>500&&clic_x<600){
    if(clic_y>50&&clic_y<80){

      quitter();
         }

	
}

  
}

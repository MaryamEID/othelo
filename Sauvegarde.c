#include <stdlib.h>
#include <stdio.h>

#include "Sauvegarde.h"


/*Sauvegarde
Sauvegarde les informations nécessaires au déroulement de la partie dans un fichier sauvegarde.txt
Prend en paramètre le plateau, la taille du plateau,le score.
Crée un fichier texte.
 */


void Sauvegarde(configuration config, int *compteur, int *profondeur, int *niveau){

  FILE *f1;
  int i,j;
  f1=fopen ("sauvegarde.txt","w");
  

 if (f1==NULL)
    {
      printf("Le fichier sauvegarde.txt n'est pas ouvert\n");
    }

 else
   {
    fprintf(f1, "%d\n",*profondeur);
     fprintf(f1, "%d\n",*niveau);
    fprintf(f1, "%d\n",*compteur);
     fprintf(f1, "%d\n",config.j1->score);
     fprintf(f1, "%d\n",config.j1->type_pions);
     fprintf(f1, "%d\n",config.j1->courant);
     fprintf(f1, "%d\n",config.j1->pions_total);
      
     fprintf(f1, "%d\n",config.j2->score);
     fprintf(f1, "%d\n",config.j2->type_pions);
     fprintf(f1, "%d\n",config.j2->courant);
     fprintf(f1, "%d\n",config.j2->pions_total);


     for (i=0;i<=7;i++){
       for(j=0;j<=7;j++){
	       fprintf(f1,"%d ",(*config.p)[i][j]);
       }
     
       fprintf(f1,"\n");
   }

   }
 fclose(f1);
}

/*Charger_sauv
Charge la sauvegarde qui se trouve dans le fichier sauvegarde.txt
Prend en paramètre le plateau, le score .
Renvoie ces paramètres avec les coordonnées qui se trouvent dans le fichier de sauvegarde.
 */

void Charger_sauv(configuration config,int *compteur, int *profondeur, int *niveau){/*ouverture et lecture du fichier de sauvegarde*/
  int i = 0;
  int j = 0;
  FILE *f1 = NULL;
  f1 = fopen("sauvegarde.txt","r+");

  if (f1 != NULL){
    fscanf(f1, "%d\n",profondeur);
    fscanf(f1, "%d\n",niveau);
    fscanf(f1, "%d\n",compteur);
     fscanf(f1, "%d\n",&config.j1->score);
     fscanf(f1, "%d\n",&config.j1->type_pions);
     fscanf(f1, "%d\n",&config.j1->courant);
     fscanf(f1, "%d\n",&config.j1->pions_total);
      
     fscanf(f1, "%d\n",&config.j2->score);
     fscanf(f1, "%d\n",&config.j2->type_pions);
     fscanf(f1, "%d\n",&config.j2->courant);
     fscanf(f1, "%d\n",&config.j2->pions_total);

     for (i=0;i<=7;i++){
       for(j=0;j<=7;j++){
	      fscanf(f1,"%d ",&(*config.p)[i][j]);
       }
    }
  }

  else{
    printf("erreur open\n");
  }
  
 fclose(f1);
}    

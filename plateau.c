/* Ce module permet la mise en place du  plateau dans lequel se déroule le jeux*/
#include <stdio.h>
#include<stdlib.h>
#include "plateau.h"

/*init_plateau
paramètres: plateau p taille du plateau n
renvoie rien*/

void init_plateau(plateau p, int n){/* initialise un plateau de n cases. Chaque case prend la valeur 0*/
  int i,j;
  for (i=0;i<n;i++){
    for (j=0;j<n;j++){
      p[i][j]=0;
    }
  }
}



/*afficher_plateau
paramètres: plateau p, taille du plateau n
renvoie rien*/

void afficher_plateau(plateau p,int n){/* parcours le plateau avec une double boucle for et affiche la valeur des cases du plateau dans le terminal*/
  int i,j,num_col=0;
  printf("    ----------------\n");
  for (i=0;i<n;i++){
    num_col++;
    printf("%d ",num_col);
    printf("| ");
    for (j=0;j<n;j++){
      if(p[i][j]==0){
	printf(". ");
      }
   
      else{
	 
	if(p[i][j]==1){ /*un pion noir occupe la case*/
	  printf("N ");
	}
	
	else{
	  printf("B "); /*un pion blanc occupe la case*/
	}
       
      }
      
    }
    printf(" |");
      printf("\n");
  }

    printf("    ----------------\n");
    printf("    A B C D E F G H\n");
}
    

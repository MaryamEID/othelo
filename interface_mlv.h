#ifndef INTERFACE_MLV_H_
#define INTERFACE_MLV_H_

#include"deroulement_partie.h"
#include<stdio.h>
#include<stdlib.h>
#include <MLV/MLV_all.h>
#include"menu.h"


/*tracer_plateau
prend en argument un plateau p
affiche le plateau dans une fenêtre graphique*/
void tracer_plateau(plateau p);

/*affiche le nombre de pion qu'il reste au joueur noir*/
int nb_pions_blanc(joueur j);

/*affiche le nombre de pion qu'il reste au joueur blanc*/
int nb_pions_noir(joueur j);

/*afficher la partie*/
coordonne afficher_coup_ordi(coordonne c,plateau p);

coordonne afficher_coup_joueur(coordonne c,plateau p);


void change_couleur(plateau p);

void score(configuration config);

void afficher_menu(int menu);

void acceder_options();

void gerer_options(configuration config, int *compteur, int *profondeur, int *niveau, int clic_x, int clic_y);


#endif

#ifndef IA_H_
#define IA_H_

#include"arbre.h"

#include<stdio.h>
#include<stdlib.h>
#include<limits.h>


int evaluation_plateau(plateau *p1,int joueur,coordonne c, int compteur);
int minmax(arbre a,int profondeur, int joueur_max, coordonne *c, int compteur);
void minimax_aux(arbre a, int profondeur, coordonne *c, int compteur);

int alphabeta(arbre a,int profondeur, int joueur_max, int *alpha, int *beta, coordonne *c,int compteur);
void alphabeta_aux(arbre a,int profondeur, int *alpha, int *beta, coordonne *c,int compteur);

int creer_arbre_position_4(arbre a,configuration config, plateau p, joueur *jr, coordonne *c, int profondeur, int compteur, int joueur_max, int *alpha, int *beta);

coordonne coup_ordinateur0(configuration config,coordonne c,int compteur);
coordonne coup_ordinateur1(configuration config, coordonne *c, int compteur);
coordonne coup_ordinateur2(configuration config, coordonne *c, int compteur, int profondeur);
coordonne coup_ordinateur3(configuration config, coordonne *c, int compteur, int profondeur);
void coup_ordinateur4(configuration config, coordonne *c, int compteur, int profondeur);

#endif

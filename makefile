OPTION = gcc -Wall -ansi -g
CFLAGS = `pkg-config --cflags MLV`
LDFLAGS = `pkg-config --libs-only-other --libs-only-L MLV`
LDLIBS = `pkg-config --libs-only-l MLV`

main: main.c arbre.o plateau.o interface_mlv.o joueur.o allocation.o deroulement_partie.o menu.o Sauvegarde.o ia.o
	$(OPTION)$(CFLAG) $(LDFLAGS) main.c arbre.o plateau.o interface_mlv.o joueur.o allocation.o deroulement_partie.o menu.o Sauvegarde.o ia.o $(LDLIBS) -o reversi

interface_mlv.o: interface_mlv.c interface_mlv.h
	 $(OPTION) $(CFLAG) $(LDFLAGS) -c interface_mlv.c $(LDLIBS)

ia.o: ia.c ia.h
	$(OPTION) -c ia.c


deroulement_partie.o: deroulement_partie.c deroulement_partie.h
	$(OPTION) -c deroulement_partie.c

menu.o: menu.c menu.h
	 $(OPTION) -c menu.c

Sauvegarde.o: Sauvegarde.c Sauvegarde.h
	$(OPTION) -c Sauvegarde.c

plateau.o: plateau.c plateau.h
	$(OPTION) -c plateau.c

joueur.o: joueur.c joueur.h
	$(OPTION) -c joueur.c

arbre.o: arbre.c arbre.h
	$(OPTION) -c arbre.c



allocation.o: allocation.c allocation.h
	$(OPTION) -c allocation.c













clean:
	rm -f *.o *~ reversi

#define N 8
#include<stdio.h>
#include<stdlib.h>

#define NOIR 1 /*humain*/
#define BLANC 2 /*ordinateur*/


typedef struct{
  int i;
  int j;
}coordonne;

typedef int plateau[N][N];/* declaration du type plateau comme tableau à double entrés*/

/*init_plateau
paramètres: plateau p taille du plateau n
renvoie rien*/
void init_plateau(plateau p,int n);/*intialise un plateau*/

/*afficheer_plateau
paramètres: plateau p, taille du plateau n
renvoie rien*/
void afficher_plateau(plateau p,int n);/*affiche le plateau */

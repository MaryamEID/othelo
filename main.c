#include"ia.h"
#include"interface_mlv.h"


int main(int argc, char **argv){

/****Déclarations****/
  int profondeur;
  int compteur=0;
  coordonne c;
  int niveau;
/*******************/

  /*type 1 = NOIR type 2 = BLANC*/


  /*Récupération de la profondeur et du niveau choisis par le joueur.
S'il ne sont pas précisés: profondeur=2 et niveau=4*/
    if (argc == 5){
	    if (strcmp(argv[1], "-prof") == 0 && strcmp(argv[3], "-niveau") == 0){
			niveau = atoi(argv[4]);
			profondeur = atoi(argv[2]);
	  	}
  	else if (argc == 3){
		if (strcmp(argv[1], "-niveau") == 0 && (strcmp(argv[2], "0") || strcmp(argv[2], "1"))){
		  niveau = atoi(argv[4]);
		  profondeur = 2;
	  	}
	}
	else{
		profondeur = 2;
		niveau = 4;
		}
	}


/*****Initialisations*******/
  joueur j1=init_joueur(1);
  joueur j2=init_joueur(2);

  configuration config;
  plateau p;
  
  init_plateau(p,8);
  
  config.p=&p;

  config.j1=&j1;
  config.j2=&j2;
  j1.courant=1;
/************************/

 
  int jeu =0;/*démarre le jeu*/
  int init=0;/*détermine à chaque itération de la boucle principale si l'on est dans le menu ou dans le jeu*/

  MLV_create_window("Reversi","Reversi",600,600);
 
	  
   while(jeu==0){

/******affichage du menu**********/
    if(init==0){
	afficher_menu(1);/*affiche le titre*/
      }
    
    else if(init!=0){
      
/*******début d'une partie*******/
      tracer_plateau(*(config.p));
      acceder_options();
      change_couleur(*(config.p));
      nb_pions_blanc(j2);
      nb_pions_noir(j1);
      
      MLV_actualise_window();
	

	/*boucle de jeu*/
    while(!est_finie(config,compteur)){
	

/*****Tour du joueur*******/
	if((j1.courant==1)){

	/*S'il n'y a pas de position gagnante, le tour du joueur est sauté*/
	  if(position_gagnante(config,compteur,j1)){

	/*Le joueur doit cliquer sur une case ou sur une des options*/
	    while((c.i<100)||(c.i>500)||(c.j<100)||(c.j>500)){
	
	      MLV_wait_mouse(&c.j,&c.i);
	      gerer_options(config,&compteur,&profondeur,&niveau,c.j,c.i);
    
	    }
       
	   /*Conversion des coordonnés en valeurs correspondant à une case du tableau*/
	    c.i=((c.i/50)-2);
	    c.j=((c.j/50)-2);
	
	/*Tant que le joueur n'a pas cliqué sur une case ou une option valide*/
	    while(!coup_valide(config, c,compteur,j1)){
	
	      while((c.i<100)||(c.i>500)||(c.j<100)||(c.j>500)){
		MLV_wait_mouse(&c.j,&c.i);
    	gerer_options(config,&compteur,&profondeur,&niveau,c.j,c.i);
	      }
       
	      c.i=((c.i/50)-2);
	      c.j=((c.j/50)-2);
      
	    }

      /*Une fois que le joueur a choisi un coup valide le jeu est mis à jour*/
	    jouer_coup(&j1,p,c);
	    change_couleur(p);
      
	    j1.pions_total--;
	    nb_pions_noir(j1);
	    afficher_coup_joueur(c,p);
	    MLV_actualise_window();
	  }	
    
	  j1.courant=0;
	  j2.courant=1;
	  compteur+=1;
	}
	
	
  
/******Tour de l'ordinateur******/
	else if(j2.courant==1){

  /*Choix du prochain coup en fonction du niveau*/
	  if(position_gagnante(config,compteur,j2)){
		  switch (niveau)
		  {
	  case 0:
			c=coup_ordinateur0(config,c,compteur);
		break;
	  case 1:
		  	c=coup_ordinateur1(config,&c,compteur);
		break;
	  case 2:
			coup_ordinateur2(config,&c,compteur,profondeur);
		break;
	  case 3:
			coup_ordinateur3(config,&c,compteur,profondeur);
		break;
		case 4:
			coup_ordinateur4(config,&c,compteur,profondeur);
		break;
		  default:
			coup_ordinateur4(config,&c,compteur,profondeur);
		  }
	       
	    jouer_coup(&j2,p,c);
	    change_couleur(p);
      
	    j2.pions_total--;
	    nb_pions_blanc(j2);
	    MLV_wait_seconds(1);
	    afficher_coup_ordi(c,p);
	    MLV_actualise_window();

	  }
	  j1.courant=1;
	  j2.courant=0;
	  compteur+=1;
	}
    
	/*A décommenter si l'on veut voir le plateau sur le terminal*/
	/*afficher_plateau(p,8);*/



	MLV_actualise_window();
	  }

	/*Fin de la partie*/
	score(config);     
	MLV_actualise_window();
	MLV_wait_seconds(3);
	init=0;


    }
	/*Affichage du menu à la fin d'une partie*/
	MLV_clear_window(MLV_COLOR_BURLYWOOD4);
    afficher_menu(1);
     MLV_actualise_window();
    
   	
   
     while(init==0) gererChoixMenu(&init,1,config,&compteur,&profondeur,&niveau);

   }
   exit(EXIT_SUCCESS);
   
}


#include"deroulement_partie.h"

/*Sauvegarde
Sauvegarde les informations nécessaires au déroulement de la partie dans un fichier sauvegarde.txt
Prend en paramètre le plateau, la taille du plateau, le serpent, la direction du serpent et le score.
Crée un fichier texte.
 */


void Sauvegarde(configuration config, int *compteur, int *profondeur, int *niveau);


/*Charger_sauv
Charge la sauvegarde qui se trouve dans le fichier sauvegarde.txt
Prend en paramètre le plateau, le serpent, le score et la direction du serpent.
Renvoie ces paramètres avec les coordonnées qui se trouvent dans le fichier de sauvegarde.
 */

void Charger_sauv(configuration config, int *compteur, int *profondeur, int *niveau);











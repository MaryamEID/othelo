#ifndef JOUEUR_H_
#define JOUEUR_H_

#include <stdlib.h>
#include <stdio.h>
#include "allocation.h"

/*définition du type joueur avec toutes les informations concernant son état*/
typedef struct joueur{
  int type_pions;
  int courant;
  int score;
  int pions_total;
  int *pos_pions_i;
  int *pos_pions_j;
}joueur;

/*initialisation d'un joueur*/
joueur init_joueur(int type);

#endif

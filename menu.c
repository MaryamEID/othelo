#include <stdio.h>
#include <stdlib.h>
#include "menu.h"
#include <MLV/MLV_all.h>


/*init1
lancement d'une partie
renvoie: rien*/
void init1(configuration config, int *compteur){/*intialise une nouvelle partie*/
    init_plateau(*(config.p),8);/* initialise le plateau*/
    config.j1->score = 0;
    config.j1->pions_total=32;
    config.j2->pions_total=32;
    config.j2->score=0;
    *compteur=0;
    }


/*init2
chargement d'une partie
renvoie: rien*/
void init2(configuration config,int *compteur, int *profondeur, int *niveau){

  Charger_sauv(config, compteur, profondeur, niveau);

}



/*gererChoixMenu
adapte l'affichage aux choix de l'utilisateur
paramètres:entiers qui définit l'état du jeu,entiers qui détermine si on est dans le menu 1(menu titre) ou menu2(tout les autres menu)(but=gerer la sauvegarde) 
renvoie rien*/
void gererChoixMenu(int *init, int menu, configuration config, int *compteur, int *profondeur, int *niveau){/*adapte l'affichage au choix de l'utilisateur*/
  int clic_x,clic_y;

    MLV_wait_mouse(&clic_x,&clic_y);
    
    if (clic_x>100&&clic_x<500){
      if(clic_y>200&&clic_y<300){/*l'utilisateur clique sur "Nouvelle partie"*/
	*init =1;/*lancer une nouvelle partie*/
	init1(config, compteur);
      }

      else if(clic_y>300 && clic_y<400){/*l'utilisateur clique sur "Charger dernière partie" */
	init2(config,compteur,profondeur, niveau);
	*init = 2;/*charger une partie*/

      }
      else if(clic_y>400 && clic_y<500){/*l'utilisateur clique sur "Quitter"*/
	quitter();
      }
  }
}

/*quitter
pas de paramètre
ne renvoie rien*/
/*permet de quitter la fenêtre de jeu*/
void quitter(){
  MLV_free_window();/*ferme la fenêtre de l'interface graphique*/
  exit(EXIT_SUCCESS);
}


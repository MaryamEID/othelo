#include <stdlib.h>
#include <stdio.h>
#include "allocation.h"

void * allocation(size_t taille,size_t nb){
    void * pointeur;
    pointeur=malloc(taille*nb);

    if(pointeur==NULL){
        printf("Erreur lors de l'allocation mémoire %ld * %ld.\n",taille,nb);
    }

    return pointeur;
}

void libere_mem(void *p){
    void ** adr_p=(void **) p;
    if(*adr_p != NULL) free(*adr_p);

    *adr_p = NULL;
}

void * alloc_0(size_t taille,size_t nb){
    void * pointeur;
    pointeur=calloc(nb,taille);

    if(pointeur==NULL){
        printf("Erreur lors de l'allocation mémoire %ld * %ld.\n",taille,nb);
    }

    return pointeur;
}

void * reallocation(void **p,size_t taille,size_t nb){
    void * pointeur;
    pointeur=realloc(*p,taille*nb);

    if(pointeur==NULL){
        printf("Erreur lors de l'allocation mémoire %ld * %ld.\n",taille,nb);
    }
    else *p=pointeur;
    return pointeur;
}

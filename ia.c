#include"ia.h"
#include<time.h>


/**
 *Niveau 0: Il n'y a pas d'IA, cette fonction choisis un coup aléatoire parmi les coups valides de l'ordinateur. 
 */
coordonne coup_ordinateur0(configuration config,coordonne c,int compteur){
  srand(time(NULL));

  int k=0;


  int *coup_possible_i=allocation(sizeof(int),64);
  int *coup_possible_j=allocation(sizeof(int),64);
  
  /*4 premiers coups*/
  if(compteur<4){
    c.i=rand()%(5-3)+3;
    c.j=rand()%(5-3)+3;

    while((*config.p)[c.i][c.j] != 0){
      c.i=rand()%(5-3)+3;
      c.j=rand()%(5-3)+3;
    }
    return c;
  }

/*En dehors des 4 premiers coups, calcul de tous les coups possibles à partir de la configuration
Tout les coups sont entrés dans un tableau*/
  for(c.i=0;c.i<N;c.i++){
    for(c.j=0;c.j<N;c.j++){

      if((*config.p)[c.i][c.j]==0 && coup_valide(config,c,compteur,*config.j2)){
        coup_possible_i[k]=c.i;
	coup_possible_j[k]=c.j;
        k++;
        
      }
    }
  }

  /*Choix d'un coup au hasard dans le tableau*/
  int coup=rand()%k;
  c.i=coup_possible_i[coup];
  c.j=coup_possible_j[coup];

  /*printf("coup ordinateur: %d %d\n",c.i,c.j);*/

  free(coup_possible_i);
  free(coup_possible_j);
  return c;
}



/*Fonction d'évaluation. Les critères d'évalution d'une position sont:
-La valeur de la case sur le plateau (les coins ont une valeur plus élevée,
 les cases autour des coins ont une valeur plus petite et ainsi de suite)
 -La mobilité, c'est à dire le nombre de coups valides à partir de cette position*/
int evaluation_plateau(plateau *p1,int joueur_max,coordonne c,int compteur){
  int val;
  int evaluation;
  plateau p;		
  init_plateau(p,N);
  configuration config;

  config.p=p1;
  joueur j1=init_joueur(1);
  joueur j2=init_joueur(2);
  config.j1=&j1;
  config.j2=&j2;
  int nb_coups=0;

  coordonne c1;

/*Valeurs des cases du plateau*/
  p[0][0]=15;
  p[0][7]=15;
  p[7][0]=15;
  p[7][7]=15;

  for(val=0;val<=2;val++){
    p[val][2]=10;
    p[5][val]=10;
    p[val][5]=10;
    p[2][val]=10;
  }

  for(val=5;val<=7;val++){
    p[2][val]=10;
    p[5][val]=10;
    p[val][5]=10;
    p[val][2]=10;
  }
  int i,j;
  for(i=0;i<=7;i++){
    for(j=0;j<=7;j++){
      if(p[i][j]==0){
	    p[i][j]=5;
      }

      /*Calcul des coups valides*/
      c1.i=i;
      c1.j=j;

      if(joueur_max==0){

	      if(compteur>4 && coup_valide(config,c1,compteur,*config.j2)){
	      nb_coups++;
	    }
    	
      }
      else if(joueur_max==1){
	      if(compteur>4 && coup_valide(config,c1,compteur,*config.j1)){
	      nb_coups++;
	    }
      }
			
    }
  }

  if (compteur>4){
    evaluation= ((p[c.i][c.j])+nb_coups)/2;
  }
  else{

    return p[c.i][c.j];
  }

  return evaluation;
}


/**Algorithme minmax. Parcours l'arbre, évalue chaque feuille et remonte la valeur de l'évaluation.
 * La branche menant à l'évaluation la plus favorable pour le joueur est la branche choisie.
  **/
int minmax(arbre a,int profondeur, int joueur_max, coordonne *c, int compteur){
  int score,score_max,i;
/*Evaluation des feuilles*/
  if(profondeur==0){
    plateau p;
    init_plateau(p,N);
    return evaluation_plateau(&a->p,joueur_max,a->c,compteur);
  }
  /*Fin de partie*/
  if(a->nb_fils==0){
    if(joueur_max==1)
      *c=a->c;
    plateau p;
    init_plateau(p,N);
		
    return evaluation_plateau(&a->p,joueur_max,a->c,compteur);
  }

/*Parcours de l'arbre*/
  if(joueur_max==1){
    score_max=INT_MIN;

    for(i=0;i<a->nb_fils;i++){
      score=minmax(a->fils[i],profondeur-1,0,c,compteur);
      if(score>score_max){
    	score_max=score;
    	*c=a->c;
      }
    }
  }

  else{
    score_max=INT_MAX;
    for(i=0;i<a->nb_fils;i++){
      score=minmax(a->fils[i],profondeur-1,1,c,compteur);
      if(score<score_max){
	    score_max=score;
	    *c=a->fils[i]->c;
      }
    }
  }

  return score_max;

}


/*Lancement de la fonction minmax. Dépend de la profondeur de l'arbre.*/
void minimax_aux(arbre a, int profondeur, coordonne *c,int compteur){
  int i, score,score_max;
  coordonne meilleur_coup;

  if(profondeur%2==0){
    score_max=INT_MAX;
    for(i=0;i<a->nb_fils;i++){
      score=minmax(a->fils[i],profondeur-1,1,c, compteur);

      if(score<score_max){
    	score_max=score;
    	meilleur_coup=a->fils[i]->c;
      }
    }	
    *c=meilleur_coup;
  }

  else{
    score_max=INT_MIN;
    for(i=0;i<a->nb_fils;i++){
      score=minmax(a->fils[i],profondeur-1,1,c, compteur);

      if(score>score_max){
    	score_max=score;
    	meilleur_coup=a->fils[i]->c;
      }
    }	
    *c=meilleur_coup;
  }
}


/**
 * Algorithme Alpha-Beta. La fonction parcours l'arbre tel la fonction Minmax 
 * mais ici les branches menant à des solutions inintéressantes ne sont pas explorées.
 **/ 
int alphabeta(arbre a,int profondeur, int joueur_max, int *alpha, int *beta, coordonne *c,int compteur){
  int score,score_max,i;

  /*Evaluation des feuilles*/
  if(profondeur==0){
    plateau p;
    init_plateau(p,N);
    return evaluation_plateau(&a->p,joueur_max,a->c,compteur);
  }

  /*Fin de partie*/
  if(a->nb_fils==0){
    if(joueur_max==1)
      *c=a->c;
    plateau p;
    init_plateau(p,N);
		
    return evaluation_plateau(&a->p,joueur_max,a->c,compteur);
  }
  /*Parcours de l'arbre*/
  if(joueur_max==1){
    score_max=INT_MIN;
    for(i=0;i<a->nb_fils;i++){

      score=alphabeta(a->fils[i],profondeur-1,0,alpha,beta,c,compteur);
      if(score>score_max){
    	score_max=score;
	    *c=a->c;
      }
      if(score_max>*alpha){
      	return score_max;
      }
      if(score_max>*alpha) *alpha = score_max;
    }
  }
  else{
    score_max=INT_MAX;
    for(i=0;i<a->nb_fils;i++){
        score=alphabeta(a->fils[i],profondeur-1,1,alpha,beta,c,compteur);
      if(score<score_max){
	      score_max=score;
	      *c=a->fils[i]->c;
      }
      if(score_max<=*alpha){
  	    return score_max;
      }
      if(*beta>score_max) *beta=score_max;
    }
  }
  return score_max;

}

/*Lancement de la fonction alpha-beta. Dépend de la profondeur de l'arbre.*/
void alphabeta_aux(arbre a,int profondeur, int *alpha, int *beta, coordonne *c, int compteur){
  int i, score,score_max;
  coordonne meilleur_coup;

  if(profondeur%2==0){
    alphabeta(a,profondeur,0,alpha,beta,c, compteur);
  }

  else{
    score_max=INT_MIN;
    for(i=0;i<a->nb_fils;i++){
      score=alphabeta(a,profondeur-1,1,alpha,beta,c,compteur);

      if(score>score_max){
	score_max=score;
	meilleur_coup=a->fils[i]->c;
      }
    }	
    *c=meilleur_coup;
  }

}





/**
 * Algorithme de création de l'arbre correspondant au niveau 4 de l'IA.
 * La création de l'arbre et son exploration sont simultanées. L'arbre n'est donc pas stocké entièrement en mémoire.
 **/
int creer_arbre_position_4(arbre a,configuration config, plateau p, joueur *jr, coordonne *c, int profondeur, int compteur, int joueur_max, int *alpha, int *beta){
  int i;
  coordonne *coup_possible=allocation(sizeof(coordonne),64);
  int score,score_max;

  plateau p2;
  init_plateau(p2,N);
    
/*Evaluation d'une position*/
  if(profondeur==0){
    plateau p;
    init_plateau(p,N);
    return evaluation_plateau(&a->p,joueur_max,a->c,compteur);
  }
  else if(profondeur>0){

    /*Le plateau du jeu en cours est copié dans la racine de l'arbre */
    memcpy(a->p,p,sizeof(plateau));

    /*Remplissage d'un tableau avec tout les coups possibles pour l'ordinateur à partir de la configuration de jeu dans la racine
      Le nombre de possibilités est renvoyé et correspond au nombre de fils de la racine*/
    a->nb_fils=remplir_arbre(&a->p,compteur,jr,coup_possible);


    if(a->nb_fils==0){
      if(joueur_max==1)
	*c=a->c;
      plateau p;
      init_plateau(p,N);
		
      return evaluation_plateau(&a->p,joueur_max,a->c,compteur);
    }



    /*Allocation de l'espace nécessaire pour les fils de la racine*/
    a->fils=(arbre *)allocation(sizeof(arbre),a->nb_fils);



    /*Pour chacun de ces fils, on copie le tableau de la racine puis on place dans le noeud le tableau qui correspond au la configuration du jeu
      si l'on joue le coup coup_possible[i]
      On place aussi comme information dans le noeud les coordonnées de ce coup, le nombre de fils=0  et le tableau fils=NULL*/
    for(i=0;i<a->nb_fils;i++){
      memcpy(p2,*a->p,sizeof(plateau));
      a->fils[i]=creer_feuille(&p2,coup_possible[i],jr);
        
    }
    /*Les coups suivants correspond aux coups du joueur humain et ainsi de suite*/
    if (jr==config.j1) jr=config.j2;
    else if (jr==config.j2) jr=config.j1;
    /*On répète le procédé ci-dessus pour les fils*/

    libere_mem(&coup_possible);


    /*Ci-dessous on applique l'algorithme alpha-beta sur les fils venant d'être créés. 
    On libère les feuilles niveau par niveau.*/
    if(joueur_max==1){
      score_max=INT_MIN;
	
      for(i=0;i<a->nb_fils;i++){
	    score=creer_arbre_position_4(a->fils[i],config, a->fils[i]->p,jr,c,profondeur-1,compteur+1,0,alpha,beta);
	    if(score>score_max){
	    score_max=score;	
      *c=a->c;
	  }
	  if(score_max>*alpha){
	    libere_mem(&a->fils[i]);
	    return score_max;
	  }
			

	  if(score_max>*alpha) *alpha = score_max;
	    libere_mem(&a->fils[i]);
      }
    }

    else{
      score_max=INT_MAX;

      for(i=0;i<a->nb_fils;i++){
	      score=creer_arbre_position_4(a->fils[i],config, a->fils[i]->p,jr,c,profondeur-1,compteur+1,1,alpha,beta);
	      if(score<score_max){
	      score_max=score;
	      *c=a->fils[i]->c;
	    }
	      if(score_max<=*alpha){
	      libere_mem(&a->fils[i]);
	      return score_max;
	      }
	      if(*beta>score_max) *beta=score_max;
	      libere_mem(&a->fils[i]);
      }
    }
    return score_max;
  }
  else{
    exit(EXIT_FAILURE);
  }
}





/**
 * Fonctions déterminant le prochain coup de l'ordinateur en fonction du niveau de l'IA.
 * Les coordonnées choisies par l'IA sont stockés dans la racine de l'arbre puis sont renvoyées par la fonction.
 **/


/**
 *Niveau 1 de l'IA. Crée un arbre de profondeur 2 puis le parcours avec l'algorithme minmax.
 **/
coordonne coup_ordinateur1(configuration config, coordonne *c, int compteur){
  coordonne coup;
  arbre a = allocation(sizeof(noeud),1);
  a->c=*c;
  a=creer_arbre_position(a,config, *config.p,config.j2,*c,2, compteur);
  minimax_aux(a,2,c,compteur);
	
  a->c=*c;
	
  coup=a->c;
  liberer_arbre(a);

  return coup;
}


/**
 *Niveau 2 de l'IA. Crée un arbre dont la profondeur est determinée par l'utilisateur puis le parcours avec l'algorithme minmax.
 **/
coordonne coup_ordinateur2(configuration config, coordonne *c, int compteur, int profondeur){
  coordonne coup;

  arbre a = allocation(sizeof(noeud),1);
  a->c=*c;
  a=creer_arbre_position(a,config, *config.p,config.j2,*c,profondeur, compteur);

  minimax_aux(a,profondeur,c, compteur);

  a->c=*c;
  coup=a->c;
  liberer_arbre(a);

  return coup;
}


/**
 *Niveau 3 de l'IA. Crée un arbre dont la profondeur est determinée par l'utilisateur puis le parcours avec l'algorithme alphabeta.
 **/
coordonne coup_ordinateur3(configuration config, coordonne *c, int compteur, int profondeur){
  coordonne coup;
  arbre a = allocation(sizeof(noeud),1);
  a->c=*c;
  a=creer_arbre_position(a,config, *config.p,config.j2,*c,profondeur, compteur);
  int alpha=INT_MIN;
  int beta=INT_MAX;
  alphabeta_aux(a,profondeur,&alpha,&beta,c,compteur);
  a->c=*c;

  coup=a->c;
  liberer_arbre(a);

  return coup;
}


/**
 *Niveau 4 de l'IA. Crée un arbre dont la profondeur est determinée par l'utilisateur puis le parcours avec l'algorithme alphabeta.
 La gestion de l'arbre est optimisée, il n'est pas stocké en entier en mémoire.
**/
void coup_ordinateur4(configuration config, coordonne *c, int compteur, int profondeur){
  arbre a = allocation(sizeof(noeud),1);
  a->c=*c;
  memcpy(a->p,*config.p,sizeof(plateau));
  int alpha=INT_MIN;
  int beta=INT_MAX;
  creer_arbre_position_4(a,config, a->p,config.j2,c,profondeur, compteur,0,&alpha,&beta);	
}

/*module menu qui propose de sauvegarder, commencer une nouvelle partie,charger une partie sauvegarder
Nécessité d'inclure le module Sauvegarde.h*/
#include <stdio.h>
#include <stdlib.h>
#include "Sauvegarde.h"


/*init1
lancement d'une partie
renvoie: rien*/

void init1(configuration config, int *compteur);

/*chargement d'une partie
renvoie: rien*/
void init2(configuration config, int *compteur, int *profondeur, int *niveau);



/*adapte l'affichage aux choix de l'utilisateur
paramètres:entiers qui définit l'état du jeu,entiers qui détermine si on est dans le menu 1(menu titre) ou menu2(tout les autres menu)(but=gerer la sauvegarde) 
renvoie rien*/
void gererChoixMenu(int *start, int menu, configuration config, int *compteur, int *profondeur, int *niveau);



/*pas de paramètre
ne renvoie rien*/
void quitter();/*permet de quitter la fenêtre de jeu*/


